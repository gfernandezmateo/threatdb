# ThreatDB


ThreatDB es una herramienta cuyo objetivo es facilitar al analista de ciberinteligencia a realiar su trabajo de una manera más efectiva.

Previa a su ejecución, se debe asegurar que se tienen instaladas las siguientes librerías:

https://pypi.org/project/colorama/
https://pypi.org/project/pymongo/
https://github.com/python/cpython/blob/3.8/Lib/pprint.py
https://pypi.org/project/requests/

Todo ello para Python 3.

Además, será necesario disponer de MongoDB en el sistema a ejecutar.

