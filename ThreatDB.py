#!/usr/bin/python

# -*- coding: utf-8 -*-

 

import os,sys
import re
import requests
import pymongo
from pprint import pprint
from colorama import init, Fore, Back, Style
from urllib.request import urlopen


myclient = pymongo.MongoClient("mongodb://localhost:27017/")
ThreatDB_database = myclient["ThreatDB"]
ThreatDB_collection = ThreatDB_database["tools"]


def menu():

	

	print ("Seleccione una opción")

	print ("\t1 - Añadir nueva fuente/herramienta")

	print ("\t2 - Buscar fuente/herramienta")

	print ("\t3 - Buscar un IOC")
	
	print ("\t4 - Ayuda")

def nuevaherramienta():
	
	

	
	name=input(Fore.YELLOW+"Nombre: "+Fore.RESET)
	
	while True:
		try:
		
			typeof=str(input(Fore.YELLOW+"¿Herramienta (H) o fuente de datos (F)?: "+Fore.RESET))
		except ValueError:
			print(Fore.YELLOW+"Debes introducir H o F"+Fore.RESET)
			continue
			
		if typeof=="H" or typeof=="F":
			break
			
			
		else:
			print(Fore.YELLOW+"Debes introducir H o F"+Fore.RESET)
	
	
	while True:
		try:
		
			languaje=str(input(Fore.YELLOW+"Idioma: "+Fore.RESET))
		except ValueError:
			print(Fore.YELLOW+"Idioma no válido. Por favor, inténtelo de nuevo:"+Fore.RESET)
			continue
			
		if languaje=="ES" or languaje=="EN" or languaje=="OTHER":
			break
			
			
		else:
			print(Fore.YELLOW+"Idioma no válido. Por favor, inténtelo de nuevo:"+Fore.RESET)
			
	
	while True:
		try:
			url=str(input(Fore.YELLOW+"URL de acceso/descarga: "+Fore.RESET))
		except ValueError:
			print(Fore.YELLOW+"Error. Por favor, inténtelo de nuevo:"+Fore.RESET)
		if url != "":
			break
		else:
			print("No se permiten direcciones en blanco.Por favor, inténtelo de nuevo: ")
	
	while True:
		try:
		
			reliability=str(input(Fore.YELLOW+"Grado de fiabilidad alto (3), medio (2), o bajo(1): "+Fore.RESET))
		except ValueError:
			print(Fore.YELLOW+"Grado no válido. Por favor, inténtelo de nuevo:"+Fore.RESET)
			continue
			
		if reliability=="3" or reliability=="2" or reliability=="1":
			break
			
			
		else:
			print(Fore.YELLOW+"Grado no válido. Por favor, inténtelo de nuevo:"+Fore.RESET)
	
	
	IOCS=str(input(Fore.YELLOW+"IOC's o temática que trabaja: "+Fore.RESET))
	
	while True:
		try:
		
			api=str(input(Fore.YELLOW+"¿Tiene API? (SI o NO): "+Fore.RESET))
		except ValueError:
			print(Fore.YELLOW+"Respuesta no válida. Por favor, inténtelo de nuevo:"+Fore.RESET)
			continue
			
		if api=="SI" or api=="NO":
			break
			
			
		else:
			print(Fore.YELLOW+"Respuesta no válida. Por favor, inténtelo de nuevo:"+Fore.RESET)
	
	tool={"name":name,"typeof":typeof,"languaje":languaje,"url":url,"reliability":reliability,"iocs":IOCS,"api":api}
	new_tool=ThreatDB_collection.insert_one(tool)
		
	

def buscarherramienta():

	print ("Seleccione el tipo de búsqueda")

	print ("\t1 - Nombre")

	print ("\t2 - Herramienta (H) o fuente (F)")

	print ("\t3 - IOC o temática")
	
	print ("\t4 - Grado de fiabilidad (alto, medio, o bajo): ")
	
	print ("\t5 - Idioma: ")

	opcionMenu2 = input("Opción elegida >> ")

 

	if opcionMenu2=="1":

		print("Introduzca el nombre a buscar: ")	
		nombrebuscado=input()
		myquery = { "name" : nombrebuscado }
		search_tool = ThreatDB_collection.find(myquery)
		for y in search_tool:
			print("-----------------------------------------------------------------------")
			print ("")
			print(Fore.RED+"Nombre: "+Fore.RESET, y['name']+Fore.RESET)
		
			print ("")
			print(Fore.YELLOW+"Tipo:"+Fore.RESET, y['typeof']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Idioma:"+Fore.RESET, y['languaje']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"URL:"+Fore.RESET, y['url']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Grado de fiabilidad:"+Fore.RESET, y['reliability']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"IOC's que trabaja o temática:"+Fore.RESET, y['iocs']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"API:"+Fore.RESET, y['api']+Fore.RESET)
			
			print ("")
			print("-----------------------------------------------------------------------")
		print(Fore.GREEN+"Intro para continuar..."+Fore.RESET)
			

	elif opcionMenu2=="2":

		print("Introduzca tipo (H ó F): ")	
		tipobuscado=input()
		myquery = { "typeof" : tipobuscado }
		search_tool = ThreatDB_collection.find(myquery)
		for y in search_tool:
			print("-----------------------------------------------------------------------")
			print ("")
			print(Fore.YELLOW+"Nombre: "+Fore.RESET, y['name']+Fore.RESET)
		
			print ("")
			print(Fore.RED+"Tipo:"+Fore.RESET, y['typeof']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Idioma:"+Fore.RESET, y['languaje']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"URL:"+Fore.RESET, y['url']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Grado de fiabilidad:"+Fore.RESET, y['reliability']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"IOC's que trabaja o temática:"+Fore.RESET, y['iocs']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"API:"+Fore.RESET, y['api']+Fore.RESET)
			
			print ("")
			print("-----------------------------------------------------------------------")
		print(Fore.GREEN+"Intro para continuar..."+Fore.RESET)
			

	elif opcionMenu2=="3":
	
		print("Introduzca IOC's: ")	
		iocbuscado=str(input())
		#var search = iocbuscado
		#myquery={"iocs": "new RegExp(.*+iocbuscado+.*)"}
		myquery = { "iocs" : { "$regex": ".*"+iocbuscado+".*" }}
		#myquery = re.findall("%s" %iocbuscado, ThreatDB_collection)
		#myquery = { "$text": { "$search": iocbuscado }}
		search_tool = ThreatDB_collection.find(myquery)
		
		for y in search_tool:
			print("-----------------------------------------------------------------------")
			print ("")
			print(Fore.YELLOW+"Nombre: "+Fore.RESET, y['name']+Fore.RESET)
		
			print ("")
			print(Fore.YELLOW+"Tipo:"+Fore.RESET, y['typeof']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Idioma:"+Fore.RESET, y['languaje']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"URL:"+Fore.RESET, y['url']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Grado de fiabilidad:"+Fore.RESET, y['reliability']+Fore.RESET)
			
			print ("")
			print(Fore.RED+"IOC's que trabaja o temática:"+Fore.RESET, y['iocs']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"API:"+Fore.RESET, y['api']+Fore.RESET)
			
			print ("")
			print("-----------------------------------------------------------------------")
		print(Fore.GREEN+"Intro para continuar..."+Fore.RESET)
	
	
	elif opcionMenu2=="4":

		print("Introduzca grado de fiabilidad: ")	
		fiabilidadbuscado=input()
		myquery = { "reliability" : fiabilidadbuscado }
		search_tool = ThreatDB_collection.find(myquery)
		for y in search_tool:
			print("-----------------------------------------------------------------------")
			print ("")
			print(Fore.YELLOW+"Nombre: "+Fore.RESET, y['name']+Fore.RESET)
		
			print ("")
			print(Fore.YELLOW+"Tipo:"+Fore.RESET, y['typeof']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Idioma:"+Fore.RESET, y['languaje']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"URL:"+Fore.RESET, y['url']+Fore.RESET)
			
			print ("")
			print(Fore.RED+"Grado de fiabilidad:"+Fore.RESET, y['reliability']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"IOC's que trabaja o temática:"+Fore.RESET, y['iocs']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"API:"+Fore.RESET, y['api']+Fore.RESET)
			
			print ("")
			print("-----------------------------------------------------------------------")
		print(Fore.GREEN+"Intro para continuar..."+Fore.RESET)
		
	elif opcionMenu2=="5":

		print("Idioma: ")	
		idiomabuscado=input()
		myquery = { "languaje" : idiomabuscado }
		search_tool = ThreatDB_collection.find(myquery)
		for y in search_tool:
			print("-----------------------------------------------------------------------")
			print ("")
			print(Fore.YELLOW+"Nombre: "+Fore.RESET, y['name']+Fore.RESET)
		
			print ("")
			print(Fore.YELLOW+"Tipo:"+Fore.RESET, y['typeof']+Fore.RESET)
			
			print ("")
			print(Fore.RED+"Idioma:"+Fore.RESET, y['languaje']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"URL:"+Fore.RESET, y['url']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"Grado de fiabilidad:"+Fore.RESET, y['reliability']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"IOC's que trabaja o temática:"+Fore.RESET, y['iocs']+Fore.RESET)
			
			print ("")
			print(Fore.YELLOW+"API:"+Fore.RESET, y['api']+Fore.RESET)
			
			print ("")
			print("-----------------------------------------------------------------------")
		print(Fore.GREEN+"Intro para continuar..."+Fore.RESET)

		

	else:

		print ("")

		input("No has pulsado ninguna opción correcta...\npulsa una tecla para continuar")

def search_ioc():

# BÚSQUEDA EN LOS FEEDS DE THREATFEEDS.IO
	IP=str(input("Introduzca la IP a buscar: "))
	url = 'https://gist.githubusercontent.com/BBcan177/bf29d47ea04391cb3eb0/raw/'
	r = requests.get(url, allow_redirects=True)
	open('ejemplo.txt', 'wb').write(r.content)
	feed_IP=open('ejemplo.txt','r')
	feed_IP2=feed_IP.read().split('\n')
	if IP in feed_IP2:
		print(Fore.YELLOW+"threatfeeds.io - BBcan177 Malicious IPs: "+Fore.RESET, "Positivo")
	else: 
		print(Fore.YELLOW+"threatfeeds.io - BBcan177 Malicious IPs: "+Fore.RESET, "Negativo")
	os.remove("ejemplo.txt")

#BÚSQUEDA EN LA API DE VIRUSTOTAL

	url = 'https://www.virustotal.com/vtapi/v2/url/report'
	api_key='079e384c94ed4b162c2d5a42b82b3e1824e013892d67289fa07cc53284fa9804'
	params = {'apikey':api_key,'resource':IP}
	response = requests.get(url, params=params)
	response_str=str(response.json())
	if "'response_code': 1" in response_str:
		print(Fore.YELLOW+"VirusTotal: "+Fore.RESET, "Positivo")
	else:
		print(Fore.YELLOW+"VirusTotal: "+Fore.RESET, "Negativo")
	
	
	
	
	

 

 

while True:

	# Mostramos el menu

	menu()

 

	# solicitamos una opción al usuario

	opcionMenu = input("inserta un numero valor >> ")

 

	if opcionMenu=="1":

		nuevaherramienta()
		

	elif opcionMenu=="2":

		buscarherramienta()

	elif opcionMenu=="3":

		search_ioc()
		
	elif opcionMenu=="4":
	
		print("Instrucciones: ")
		print("")
		print("Opción 1 - Añadir nueva fuente/herramienta:\n\nMediante esta opción podemos añadir nuevas herramientas o fuentes de datos para tener en nuestra base de datos. En alguno de los campos existe validación de los datos a introdución. La función de cada uno es la siguiente: \n\n Nombre: campo para indicar el nombre de la herramienta.\n\nTipo: campo para inidcar si es una herramienta/programa (H) o una fuente de datos, como pueda ser una web de información.Se debe introducir H o F según el tipo, no se admite otra respuesta\n\nIdioma: campo para introducir el idioma de la herramienta o la fuente correspondiente. Hay 3 opciones, ES (Español), EN (Inglés) y OTHER (otros idiomas)\n\nURL: campo para introducir la URL de acceso o descarga a la herramienta o la fuente de datos.\n\nGrado de fiabilidad: campo para indica si el grado de fiabilidad es alto (3), medio(2), o bajo(1). Se deben introducir los correspondientes números.\n\nIOC's que trabaja o temática: campo para introducir el tipo de información que trata la fuente/herramienta.\n\n API: campo para indicar si dicha fuente/herramienta cuenta con una API para consultar los datos.")
		print("-----------------------------------------------------------------")
		print("Opción 2 - Buscar fuente/herramienta:\n\n Mediante esta opción es posible hacer búsquedas de las herramientas que coinciden conn cada uno de los criterios indicados")
		print("-----------------------------------------------------------------")
		print("Opción 3 - Buscar un IOC:\n\n En esta opción podemos introducir un IOC y ver qué resultado nos ofrecen las distintas API integradas en nuestra aplicación\n\n")
		

	else:

		print ("")

		input("No has pulsado ninguna opción correcta...\npulsa una tecla para continuar")
